import java.util.*
import java.util.concurrent.ThreadLocalRandom

class Cashier(
        var id: String = UUID.randomUUID().toString(),
        val full_name: String = "",
        birthdate: Date = Date(ThreadLocalRandom.current().nextLong(946684800000L)),
        val salary: Int = ThreadLocalRandom.current().nextInt(10, 100) * 1000
) {

    val birthdate = Calendar.getInstance().run {
        time = birthdate
        set(Calendar.HOUR_OF_DAY, 0)
        set(Calendar.HOUR, 0)
        set(Calendar.AM_PM, 0)
        set(Calendar.MINUTE, 0)
        set(Calendar.SECOND, 0)
        set(Calendar.MILLISECOND, 0)
        time
    }

    override fun toString(): String {
        return "Cashier(id='$id', full_name='$full_name', salary=$salary, birthdate=$birthdate)"
    }

}

data class Dish(
        var id: String = UUID.randomUUID().toString(),
        val name: String = "",
        val price: Float = ThreadLocalRandom.current().nextInt(1,300).toFloat()
) {

    override fun toString(): String {
        return "Dish(id='$id', name='$name', price=$price)"
    }
}

class Shift(
        var id: String = UUID.randomUUID().toString(),
        val type: Int = ThreadLocalRandom.current().nextInt(5),
        date: Date = Date(ThreadLocalRandom.current().nextLong(1514764800000L, 1525132800000L)),
        val cashiers: List<ShiftCashier> = emptyList()
) {

    val date: Date = Calendar.getInstance().run {
        time = date
        set(Calendar.HOUR_OF_DAY, 0)
        set(Calendar.HOUR, 0)
        set(Calendar.AM_PM, 0)
        set(Calendar.MINUTE, 0)
        set(Calendar.SECOND, 0)
        set(Calendar.MILLISECOND, 0)
        time
    }

    override fun toString(): String {
        return "Shift(id='$id', type=$type, cashiers=$cashiers, date=$date)"
    }

    data class ShiftCashier(val cashierId: String, val cashboxId: String)

}

class Order(
        var id: String = UUID.randomUUID().toString(),
        val price: Float = ThreadLocalRandom.current().nextInt(50, 1500).toFloat(),
        val date: Date = Date(ThreadLocalRandom.current().nextLong(1514764800000L, 1525132800000L)),
        val dishes: List<OrderDish> = emptyList()
) {

    override fun toString(): String {
        return "Order(id='$id', price=$price, date=$date, dishes=$dishes)"
    }

    data class OrderDish(val dishId: String, val amount: Int, val price: Float)

}

class Composition(
        var id: String
) {

    override fun toString(): String {
        return "Composition(id='$id')"
    }
}

class Shelf(
        var id: String = UUID.randomUUID().toString(),
        val compositionId: String,
        val capacity: Int = ThreadLocalRandom.current().nextInt(2, 41)
) {

    override fun toString(): String {
        return "Shelf(id='$id', compositionId='$compositionId', capacity=$capacity)"
    }
}

class ShelfDish(
        var id: String = UUID.randomUUID().toString(),
        val dishId: String,
        val shelfId: String,
        val shelf_life: Int = ThreadLocalRandom.current().nextInt(5, 10080)
) {

    override fun toString(): String {
        return "ShelfDish(id='$id', dishId='$dishId', shelf=$shelfId, shelf_life=$shelf_life)"
    }
}