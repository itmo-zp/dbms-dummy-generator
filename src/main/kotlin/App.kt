import io.codearte.jfairy.Fairy
import java.util.*
import khttp.responses.Response
import org.json.JSONObject
import java.text.SimpleDateFormat

private const val API_URL = "http://35.237.53.82:3000/graphql"
private val DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'")

private const val SHIFTS_MAX_ITERATIONS = 20

object App {

    @JvmStatic
    fun main(args: Array<String>) {
        if (args.size < 9) {
            throw IllegalArgumentException("Should have as least 9 args")
        }

        val cashiers = mutableListOf<Cashier>()
        val dishes = mutableListOf<Dish>()
        val shifts = mutableListOf<Shift>()
        val orders = mutableListOf<Order>()
        val cashboxes = mutableListOf<String>()

        val compositions = mutableListOf<Composition>()
        val shelfs = mutableListOf<Shelf>()
        val shelfDishes = mutableListOf<ShelfDish>()

        val fairy = Fairy.create()
        val random = Random()

        for (i in 1..args[0].toInt()) {
            cashiers.add(
                    addCashier(Cashier(full_name = fairy.person().fullName))
            )
        }

        System.out.println("Cashiers inserted")

        for (i in 1..args[1].toInt()) {
            cashboxes.add(
                    addCashbox()
            )
        }

        System.out.println("Cashboxes inserted")

        for (i in 1..args[2].toInt()) {
            var dishName = ""
            do {
                dishName = fairy.textProducer().latinWord(random.nextInt(4) + 1).run {
                    substring(0, 1).toUpperCase() + substring(1)
                }
            } while ((dishName.length < 4) || dishes.any { it.name == dishName })

            dishes.add(
                    addDish(Dish(name = dishName))
            )
        }

        System.out.println("Dishes inserted")

        for (i in 1..args[3].toInt()) {
            val shiftCashiers0 = mutableListOf<Shift.ShiftCashier>()
            var cashboxIdIndex0 = random.nextInt(cashboxes.size)
            var count = 0
            for (j in random.nextInt(cashiers.size)..cashiers.lastIndex) {
                shiftCashiers0.add(
                        Shift.ShiftCashier(cashiers[j].id, cashboxes[cashboxIdIndex0])
                )
                cashboxIdIndex0 = (cashboxIdIndex0 + 1) % cashboxes.size
                if (++count > SHIFTS_MAX_ITERATIONS) break
            }
            val shift0 = Shift(type = 0, cashiers = shiftCashiers0)

            val shiftCashiers1 = mutableListOf<Shift.ShiftCashier>()
            var cashboxIdIndex1 = random.nextInt(cashboxes.size)
            count = 0
            for (j in random.nextInt(cashiers.size)..cashiers.lastIndex) {
                shiftCashiers1.add(
                        Shift.ShiftCashier(cashiers[j].id, cashboxes[cashboxIdIndex1])
                )
                cashboxIdIndex1 = (cashboxIdIndex1 + 1) % cashboxes.size
                if (++count > SHIFTS_MAX_ITERATIONS) break
            }
            val shift1 = Shift(type = 1, date = shift0.date, cashiers = shiftCashiers1)

            shifts.add(
                    addShift(shift0)
            )
            shifts.add(
                    addShift(shift1)
            )
        }

        System.out.println("Shifts inserted")

        for (i in 1..args[4].toInt()) {
            val orderDishes = mutableListOf<Order.OrderDish>()
            var dishesAmount = random.nextInt(5) + 1
            for (j in 1..dishesAmount) {
                var dish: Dish
                do {
                    dish = dishes[random.nextInt(dishes.size)]
                } while (orderDishes.any { it.dishId == dish.id })
                orderDishes.add(Order.OrderDish(dish.id, random.nextInt(3) + 1, dish.price))
            }

            orders.add(
                    addOrder(Order(dishes = orderDishes, price = orderDishes.sumByDouble { it.price.toDouble() }.toFloat()))
            )
        }

        System.out.println("Orders inserted")

        for (i in 1..args[5].toInt()) {
            compositions.add(addComposition())
        }

        System.out.println("Compositions inserted")

        for (i in 1..args[6].toInt()) {
            shelfs.add(
                    addShelf(
                            Shelf(compositionId = compositions[random.nextInt(compositions.size)].id)
                    )
            )
        }

        System.out.println("Shelfs inserted")

        for (i in 1..args[7].toInt()) {
            shelfDishes.add(
                    addShelfDish(
                            ShelfDish(dishId = dishes[random.nextInt(dishes.size)].id, shelfId = shelfs[random.nextInt(shelfs.size)].id)
                    )
            )
        }

        System.out.println("ShelfDishes inserted")

        for (i in 1..args[8].toInt()) {
            addToCashboxQueue(
                    cashboxes[random.nextInt(cashboxes.size)]
            )
        }

        System.out.println("Queues filled")


        System.out.println("DONE")

    }

    fun addDish(dish: Dish): Dish {
        val funcName = "addDish"
        val dishInput = "dish: {name:\"${dish.name}\", price: ${dish.price}}"
        dish.id = ((request(true, funcName, dishInput, "id").jsonObject.get("data") as? JSONObject)?.get(funcName) as? JSONObject)?.get("id")?.toString().orEmpty()
        return dish
    }

    fun addCashier(cashier: Cashier): Cashier {
        val funcName = "addCashier"
        val cashierInput = "cashier: {full_name:\"${cashier.full_name}\", birthdate: \"${DATE_FORMAT.format(cashier.birthdate)}\", salary: ${cashier.salary}}"
        cashier.id = ((request(true, funcName, cashierInput, "id").jsonObject.get("data") as? JSONObject)?.get(funcName) as? JSONObject)?.get("id")?.toString().orEmpty()
        return cashier
    }

    fun addCashbox(): String {
        val funcName = "addCashbox"
        return ((request(true, funcName, "", "id").jsonObject.get("data") as? JSONObject)?.get(funcName) as? JSONObject)?.get("id")?.toString().orEmpty()
    }

    fun addShift(shift: Shift): Shift {
        var cashiers = "["
        shift.cashiers.forEachIndexed { index, shiftCashier ->
            if (index > 0) {
                cashiers += ","
            }
            cashiers += "{cashierId:\"${shiftCashier.cashierId}\", cashboxId:\"${shiftCashier.cashboxId}\"}"
        }
        cashiers += "]"
        val funcName = "addShift"
        val cashierInput = "shift: {type:${shift.type}, date: \"${DATE_FORMAT.format(shift.date)}\", cashiers: $cashiers}"
        shift.id = ((request(true, funcName, cashierInput, "id").jsonObject.get("data") as? JSONObject)?.get(funcName) as? JSONObject)?.get("id")?.toString().orEmpty()
        return shift
    }

    fun addOrder(order: Order): Order {
        var dishes = "["
        order.dishes.forEachIndexed { index, dish ->
            if (index > 0) {
                dishes += ","
            }
            dishes += "{dishId:\"${dish.dishId}\", amount:${dish.amount}}"
        }
        dishes += "]"
        val funcName = "addOrder"
        val orderInput = "order: {price:${order.price}, date: \"${DATE_FORMAT.format(order.date)}\", dishes: $dishes}"
        order.id = ((request(true, funcName, orderInput, "id").jsonObject.get("data") as? JSONObject)?.get(funcName) as? JSONObject)?.get("id")?.toString().orEmpty()
        return order
    }

    fun addComposition(): Composition {
        val funcName = "addComposition"
        val id = ((request(true, funcName, "", "id").jsonObject.get("data") as? JSONObject)?.get(funcName) as? JSONObject)?.get("id")?.toString().orEmpty()
        return Composition(id)
    }

    fun addShelf(shelf: Shelf): Shelf {
        val funcName = "addShelf"
        val shelfInput = "shelf: {capacity:${shelf.capacity}}, compositionId: \"${shelf.compositionId}\""
        shelf.id = ((request(true, funcName, shelfInput, "id").jsonObject.get("data") as? JSONObject)?.get(funcName) as? JSONObject)?.get("id")?.toString().orEmpty()
        return shelf
    }

    fun addShelfDish(shelfDish: ShelfDish): ShelfDish {
        val funcName = "addShelfDishFirst"
        val dishInput = "shelfDish: {dish: \"${shelfDish.dishId}\", shelf_life: ${shelfDish.shelf_life}}, shelfId: \"${shelfDish.shelfId}\""
        shelfDish.id = ((request(true, funcName, dishInput, "id").jsonObject.get("data") as? JSONObject)?.get(funcName) as? JSONObject)?.get("id")?.toString().orEmpty()
        return shelfDish
    }

    fun addToCashboxQueue(cashboxId: String): String {
        val funcName = "addToQueue"
        val input = "cashboxId: \"$cashboxId\""
        return (request(true, funcName, input, "").jsonObject["data"] as? JSONObject)?.get(funcName)?.toString().orEmpty()
    }

    fun request(isMutation: Boolean, function: String, args: String, returnParams: String): Response = khttp.post(
            url = API_URL,
            json = mapOf("query" to "${if (isMutation) "mutation" else ""}{" +
                    function +
                    (if (args.isEmpty()) "" else "($args)") +
                    (if (returnParams.isEmpty()) "" else "{$returnParams}") +
                    "}")
    )

}